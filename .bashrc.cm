# Aliases / scripts for CM work

export PATH="$PATH:/opt/toolchains/linaro-4.8/bin/"

alias sync-cm='( cd $CM_SRC && repo sync 2>&1 | tee /tmp/sync.txt ; cp /tmp/sync.txt ~/syncs-$CM_WHICH/`date +%Y%m%dT%H%M%S` ; echo ; echo "*** ERRORS ***" ; echo ; egrep "error|leaving |CONFLICT" /tmp/sync.txt )'

alias start-cm-11.0="export CM_WHICH=cm-11.0 CM_SRC=~/cm-11.0 && cd ~/cm-11.0"
alias start-cm-12.0="export CM_WHICH=cm-12.0 CM_SRC=~/cm-12.0 && cd ~/cm-12.0"

start-cm-12.0

build-cm() {
(
  export OUT_DIR_COMMON_BASE=/out/$1/ && \
  echo $OUT_DIR_COMMON_BASE && \
  cd $CM_SRC && \
  . ./build/envsetup.sh && \
  brunch $1 && \
  if [ -e /mnt/roms ]; then
    mkdir -p /mnt/roms/$1 && \
    cp $OUT_DIR_COMMON_BASE/$CM_WHICH/target/product/$1/*-UNOFFICIAL*-$1.zip /mnt/roms/$1
  fi
) 2>&1 | tee /tmp/build-$1.txt
}

start-cm() {
  if [ "$BASE_PS1" = "" ]; then
     export BASE_PS1="$PS1"
  fi
  export PS1="[$1] $BASE_PS1"

  export OUT_DIR_COMMON_BASE=/out/$1/
  cd $CM_SRC
  . ./build/envsetup.sh
  breakfast $1

  export OUT="$OUT_DIR_COMMON_BASE/$CM_WHICH/target/product/$1"

  case "$1" in
  dlx)
    export ANDROID_SERIAL=FA2BLS506023
    ;;
  hammerhead)
    export ANDROID_SERIAL=03a3e4ad437cbd9b
    ;;
  mondrianwifi)
    export ANDROID_SERIAL=1019440c06792b29
    ;;
  picassolte)
    export ANDROID_SERIAL=1ed18fed
    ;;
  picassowifi)
    export ANDROID_SERIAL=32044d1a104eb0db
    ;;
  victara)
    export ANDROID_SERIAL=TA98903JFQ
    ;;
  *)
    echo "*** Current using device: $ANDROID_SERIAL"
    ;;
  esac

  CM_DEVICE=$1
}

alias start-dlx="start-cm dlx"
alias start-hammerhead="start-cm hammerhead"
alias start-mondrianwifi="start-cm mondrianwifi"
alias start-picassolte="start-cm picassolte"
alias start-picassowifi="start-cm picassowifi"
alias start-victara="start-cm victara"

logcat() {
   adb logcat -v time | tee /tmp/lc.$CM_DEVICE
}

release-cm() {
  export OUT_DIR_COMMON_BASE=/out/$1/ && \
  scp `ls -t $OUT_DIR_COMMON_BASE/$CM_WHICH/target/product/$1/*-UNOFFICIAL-$1.zip | head -1` download.crpalmer.org:/downloads/$1/
}

release-nightly() {
  export OUT_DIR_COMMON_BASE=/out/$1/
  D=/nightlies/$1-$CM_WHICH
  if [ "`ssh download.crpalmer.org ls -d $D`" = "" ]; then
    D=/nightlies/$i
    ssh download.crpalmer.org mkdir -p $D
  fi
  F=`ls -t $OUT_DIR_COMMON_BASE/$CM_WHICH/target/product/$1/*-UNOFFICIAL-$1.zip | head -1`
  echo "Releasing $F"
  echo "To        $D"
  scp $F download.crpalmer.org:$D/
}

build-cm-boot() {
(
  export OUT_DIR_COMMON_BASE=/out/$1 && \
  echo $OUT_DIR_COMMON_BASE && \
  cd $CM_SRC && \
  . ./build/envsetup.sh && \
  breakfast $1 && \
  mka bootimage && \
  boot-odin $1
) 2>&1 | tee /tmp/build-$1.txt
}

build-cm-boot-odin() {
  build-cm-boot $1 && \
  boot-odin $1
}

clean-cm() {
(
  export OUT_DIR_COMMON_BASE=/out/$1 && \
  cd $CM_SRC && \
  . ./build/envsetup.sh && \
  make clean
)
}

boot-odin() {
(
  cd /mnt/roms/odin && \
  create-odin cm-boot-$1 /out/$1/$CM_WHICH/target/product/$1/boot.img
)
}

gdb-cm() {
  export OUT_DIR_COMMON_BASE=/out/$1 && \
  . ./build/envsetup.sh && \
  breakfast $1 && \
  gdbclient
}

# -------------------------

alias build-victara='build-cm victara'
alias build-victara-boot='build-cm-boot victara'
alias release-victara='release-cm victara'

alias build-mondrianwifi='build-cm mondrianwifi'
alias build-mondrianlte='build-cm mondrianlte'
alias build-mondrianwifi-boot='build-cm-boot-odin mondrianwifi'
alias build-mondrianlte-boot='build-cm-boot-odin mondrianlte'
alias mondrianwifi-boot-odin='boot-odin mondrianwifi'
alias mondrianlte-boot-odin='boot-odin mondrianlte'
alias release-mondrianlte='release-cm mondrianlte'

alias build-picassolte='build-cm picassolte'
alias build-picassolte-boot='build-cm-boot-odin picassolte'
alias picassolte-boot-odin='boot-odin picassolte'
alias release-picassolte='release-cm picassolte'

alias build-picassowifi='build-cm picassowifi'
alias build-picassowifi-boot='build-cm-boot-odin picassowifi'
alias picassowifi-boot-odin='boot-odin picassowifi'
alias release-picassowifi='release-cm picassowifi'

alias build-dlx='build-cm dlx'
alias build-dlx-boot='build-cm-boot dlx'
alias release-dlx='release-cm dlx'

alias build-hh='build-cm hammerhead'
alias build-hh-boot='build-cm-boot hammerhead'
alias release-hh='release-cm hammerhead'
alias build-hammerhead='build-cm hammerhead'
alias build-hammerhead-boot='build-cm-boot hammerhead'
alias release-hammerhead='release-cm hammerhead'

alias gdb-mondrianwifi="gdb-cm mondrianwifi"

alias mondrianwifi-recovery-odin='( cd /mnt/roms/odin && create-odin cm-recovery-mondrianwifi /out/mondrianwifi/*/target/product/mondrianwifi/recovery.img )'

alias picassolte-recovery-odin='( cd /mnt/roms/odin && create-odin cm-recovery-picassolte /out/picassolte/*/target/product/picassolte/recovery.img )'

alias picassowifi-recovery-odin='( cd /mnt/roms/odin && create-odin cm-recovery-picassowifi /out/picassowifi/*/target/product/picassowifi/recovery.img )'

# -------------------------

cm-status() {
(
    cd $CM_SRC
    for f in `repo status | sed "s/^project //p;d" | sed "s/ .*//" | egrep -v '^prebuilts/' | sort`
    do
        (
             cd $f

	     upstream=`repo info . | sed 's/Manifest merge branch: //p;d'`
	     revision=`repo info . | sed 's/Current revision: //p;d'`
	     if echo $revision | egrep -q refs/heads/ ; then
		:
	     else
	        upstream=$revision
	     fi

	     changes=`( git status --porcelain ; log github/$upstream.. ) 2>&1 | wc -l`
	     if [ "$changes" != 0 ]; then
                 echo
                 echo "******* $f: $upstream: $changes updates *****"
                 echo
                 log github/$upstream..
                 git status --porcelain
	     fi
        ) 2>&1
   done
) | less
}

# ------------------------

flash-boot() {
    boot=/out/$1/$CM_WHICH/target/product/$1/boot.img
    if [ ! -r "$boot" ]; then
	echo >&2 "error: Must specify build target and build it.  Failed to access:\n\t$boot"
    else
	dev=dw_mmc.0
        if [ "$2" != "" ]; then
	    dev="$2"
        else
	    dev="/dev/block/platform/dw_mmc.0/by-name/BOOT"
        fi
        adb push $boot /sdcard/boot.img
        adb shell su -c "dd if=/sdcard/boot.img of=$dev"
	adb reboot
    fi
}

flash-boot-fastboot() {
    boot=/out/$1/$CM_WHICH/target/product/$1/boot.img
    fastboot flash boot $boot && fastboot reboot
}

alias flash-boot-dlx='flash-boot dlx'
alias flash-boot-hammerhead='flash-boot hammerhead'
alias flash-boot-mondrianwifi='flash-boot mondrianwifi /dev/block/platform/msm_sdcc.1/by-name/boot'
alias flash-boot-picassolte='flash-boot picassolte /dev/block/platform/msm_sdcc.1/by-name/boot'
alias flash-boot-picassowifi='flash-boot picassowifi'
alias flash-boot-victara='flash-boot-fastboot victara'

# ------------------------

flash-recovery() {
   recovery=/out/$1/*/target/product/$1/recovery.img
   if [ ! -r $recovery ]; then
	echo >&2 "error: Must specify build target and build it.  Failed to access:\n\t$recovery"
	exit 1
   fi
   if [ "$2" != "" ]; then
	dev="$2"
   else
	dev="/dev/block/platform/dw_mmc.0/by-name/RECOVERY"
   fi
   adb push $recovery /sdcard/recovery.img
   adb shell su -c "dd if=/sdcard/recovery.img of=$dev"
}

alias flash-recovery-dlx='flash-recovery dlx'
alias flash-recovery-hammerhead='flash-recovery hammerhead'
alias flash-recovery-mondrianwifi='flash-recovery mondrianwifi /dev/block/platform/msm_sdcc.1/by-name/recovery'
alias flash-recovery-picassolte='flash-recovery picassolte'
alias flash-recovery-picassowifi='flash-recovery picassowifi'

# -----------------------------

audit2allow-cm()
{
  export OUT_DIR_COMMON_BASE=/out/$1 && \
  audit2allow -p $OUT_DIR_COMMON_BASE/$CM_WHICH/target/product/$1/root/sepolicy
}

alias audit2allow-dlx='audit2allow-cm dlx'
alias audit2allow-dlx-all='(adb shell dmesg ; adb shell su -c "cat /data/misc/audit/*" ) | tee /tmp/audit2allow | audit2allow-cm dlx'
alias audit2allow-mondrianwifi='audit2allow-cm mondrianwifi'
alias audit2allow-mondrianwifi-all='(adb shell dmesg ; adb shell su -c "cat /data/misc/audit/*" ) | tee /tmp/audit2allow | audit2allow-cm mondrianwifi'
alias audit2allow-picassolte='audit2allow-cm picassolte'
alias audit2allow-picassolte-all='(adb shell dmesg ; adb shell su -c "cat /data/misc/audit/*" ) | tee /tmp/audit2allow | audit2allow-cm picassolte'
alias audit2allow-picassowifi='audit2allow-cm picassowifi'
alias audit2allow-picassowifi-all='(adb shell dmesg ; adb shell su -c "cat /data/misc/audit/*" ) | tee /tmp/audit2allow | audit2allow-cm picassowifi'
alias audit2allow-victara='audit2allow-cm victara'
alias audit2allow-victara-all='(adb shell dmesg ; adb shell su -c "cat /data/misc/audit/*" ) | tee /tmp/audit2allow | audit2allow-cm victara'

# -----------------------------

tis-delta() {
    date
    adb shell su -c "cat $1" | dos2unix > /tmp/tis
    join -j 1 -t ' ' /tmp/tis.last /tmp/tis | \
	awk '{ if ($3 != $2) printf("%d %d\n", $1, $3 - $2); }'
    cp /tmp/tis /tmp/tis.last
}

alias tis-delta-mali="tis-delta /sys/devices/platform/mali.0/time_in_state"
alias tis-delta-cpu0="tis-delta /sys/devices/system/cpu/cpu0/cpufreq/stats/time_in_state"
alias tis-delta-cpu1="tis-delta /sys/devices/system/cpu/cpu1/cpufreq/stats/time_in_state"
alias tis-delta-cpu2="tis-delta /sys/devices/system/cpu/cpu2/cpufreq/stats/time_in_state"
alias tis-delta-cpu3="tis-delta /sys/devices/system/cpu/cpu3/cpufreq/stats/time_in_state"

alias restart-adb='adb kill-server && sudo `which adb` start-server'
adb-kill() {
   adb shell su -c "kill `adb shell ps | grep $* | cut -c 9-15`"
}

generate-changelog() {
  cd $CM_SRC/$1
  TAG=nightly-`date +%Y%m%d`
  git tag -d $TAG 2>/dev/null
  git tag $TAG
  git log --oneline --decorate
}

tag-finder() {
    best=9999999999
    tag=""

    for commit in $(git tag -l $1)
    do
        match=$(git diff $commit --shortstat | awk -F' ' '{print $4 + $6}')
        echo $commit has $match lines changed

        if [ $match -lt $best ]; then
            best=$match
            tag=$commit
            echo $tag is the new best match with $best lines changed
        fi
    done

    echo "Best match"
    echo TAG : $tag
    echo Lines Changed : $best
}

alias gc-prima='gc --strategy=subtree --strategy-option subtree=drivers/staging/prima'
